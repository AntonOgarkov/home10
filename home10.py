

class worker:
    def __init__(self, n='',a=0,k=0,s=0,p=''):
        self._name = n
        self._age = int(a)
        self._k = int(k)
        self._salary = int(s)
        self._position = p

    @property
    def name(self):
        return self._name
    @name.setter
    def name(self, val):
        self._name = val
        return self._name

    @property
    def k(self):
        return self._k
    @k.setter
    def k(self, val):
        self._k = val
        return self._k

    @property
    def salary(self):
        return self._salary
    @salary.setter
    def salary(self, val):
        self._salary = val
        return self._salary

    @property
    def age(self):
        return self._age
    @age.setter
    def age(self, val):
        self._age = val
        return self._age

    @property
    def position(self):
        return self._position
    @position.setter
    def position(self, val):
        self._position = val
        return self._position

    def money(self,days,hours):
        return self._salary *days*hours
    def rest(self):
        return 65-self._age



class database:
    def __init__(self):
        self._workerDict = {}
    def worker(self, key):
        return self._workerDict[key]
    def add_worker(self, key, w = worker):
        self._workerDict.update({key : w})
    def del_worker(self,key):
        _ = self._workerDict.pop(key)
    @property
    def workers(self):
        return len(self._workerDict)
    def load_db(self,path):
        f = open(path, 'r')
        for l in f.readlines():
            # print(l)
            q = l[:-2].split(' ')
            if q != '':
                # print(q)
                self.add_worker(len(self._workerDict),worker(*q))
    def save_db(self, path):
        f = open(path,'w')
        for i in self._workerDict.values():
            l = dict(i.__dict__).values()
            for v in l:
                f.write(str(v)+' ')
            f.write('\n')



if __name__ == '__main__':
    w = worker('',20,0,0,'indus')
    w.name = 'anton'
    d = database()
    d.add_worker(1,w)
    print(d._workerDict[1].__dict__)
    d.save_db('db.txt')
    q = database()
    q.load_db('db.txt')
    print(q._workerDict[0].__dict__)
    print(w.rest())


